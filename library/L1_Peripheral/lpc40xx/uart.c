#include "uart.h"
#include "utility/log.hpp"


static LPC_UART_TypeDef* UART[3] = {LPC_UART0,LPC_UART2,LPC_UART3};

/*
    UART[2] -> LCR |=3;
*/

static PCONP_typedef PCONP_reg;
static LCR_typedef LCR_reg;


/*
struct UartCalibration_t
{
  uint32_t divide_latch = 0;
  uint32_t divide_add   = 0;
  uint32_t multiply     = 1;
};

static UartCalibration_t FindClosestFractional(float decimal)
{
  UartCalibration_t result;
  bool finished = false;
  for (int div = 0; div < 15 && !finished; div++)
  {
    for (int mul = div + 1; mul < 15 && !finished; mul++)
    {
      float divf         = static_cast<float>(div);
      float mulf         = static_cast<float>(mul);
      float test_decimal = 1.0f + divf / mulf;
      if (decimal <= test_decimal + kThreshold &&
          decimal >= test_decimal - kThreshold)
      {
        result.divide_add = div;
        result.multiply   = mul;
        finished          = true;
      }
    }
  }
  return result;
}
*/
void l1_uart__init(uart_port_e port, uint32_t baudrate)
{       
	PCONP_reg.PCONP = LPC_SC ->PCONP;
	switch(port)
	{
		case(uart0): PCONP_reg.PCUART0 = 1;
		             break;
		case(uart2): PCONP_reg.PCUART2 = 1;
		             break;
		case(uart3): PCONP_reg.PCUART3 = 1;
		             break;
		default:    PCONP_reg.PCUART3 = 1;
		              break;
	}
	power_on_peripheral(PCONP_reg);

        //set peripheral clock
        //LPC_SC ->PCLKSEL |= PCLK_DIVIDE_BY_3;
         
        LCR_reg.WLS = BIT_CHAR_LENGTH_8;
        LCR_reg.SBS = STOP_BIT_1;;
        LCR_reg.DLAB = 1;

        //configure LCR for word length, stop bit and enable DLAB
        UART[port] -> LCR = (LCR_reg.LCR);
 
        //set baud rate to 9600
        //TODO: create a formula for calculating baud rate to the nearest decimal

        uint32_t var_RegValue_u32 = (PCLK / (16 * baudrate ));
        UART[port] -> DLL =  var_RegValue_u32 & 0xFF;
        UART[port] -> DLM =  (var_RegValue_u32 >> 0x08) & 0xFF;

        LOG_INFO("%u, %u\n", UART[port]->DLL, UART[port]-> DLM);
        LCR_reg.DLAB = 0;
        UART[port] -> LCR = LCR_reg.LCR; 
#if 1
        //enable FIFO
        UART[port] -> FCR |= FIFORESET_ENABLE;

        //select pins
        LPC_IOCON -> P4_29 |= 2; //RX
        LPC_IOCON -> P4_28 |= 2; //TX   
     
        //Enable UART interrupts
       // UART[port] -> IER |= 0x03; //Enables the receive and empty THR register
#endif
}

bool l1_uart__getchar(uart_port_e port, char *buffer)
{
     if((UART[port] -> LSR & 1))
     {
         *buffer = UART[port] ->RBR; 
          return true; 
     }
     return false;
}

void l1_uart__putchar(uart_port_e port, const char src)
{
     if((UART[port] -> LSR & (1 << 5)))
     {
          UART[port] ->THR = src;
          return; 
     }
}


