#pragma once
#include "utility/bitfields.h"
#include "L0_Platform/lpc40xx/LPC40xx.h"

#ifdef __cplusplus
extern "C"{
#endif

void power_on_peripheral(PCONP_typedef pconfig);

#ifdef __cplusplus
}
#endif
