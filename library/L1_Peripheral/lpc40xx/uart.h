#pragma once
#include <stdbool.h>
#include "L0_Platform/lpc40xx/LPC40xx.h"
#include "system_controller.h"
#include "utility/bitfields.h"
#include "macros.h"

#ifdef __cplusplus
extern "C"{
#endif

//The only uart pin exposed on SJ2 is UART3 at PIN4_28,4_29
//The pinvalue is hardcoded in the driver

//TODO: Make UART generic for different pins 

typedef enum  
{
    uart0,
    uart2,
    uart3
}uart_port_e;

bool l1_uart__open();

void l1_uart__init(uart_port_e,uint32_t baudrate);

bool l1_uart__getchar(uart_port_e, char *buff);

void l1_uart__putchar(uart_port_e, const uint8_t* data);

#ifdef __cplusplus
}
#endif

