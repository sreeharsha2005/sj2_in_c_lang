#pragma once

#include<stdint.h>
typedef union{
    uint32_t PCONP;
    struct{
    	uint32_t PCLCD:1,
    	PCTIM0:1,
    	PCTIM1:1,
    	PCUART0:1,
    	PCUART1:1,
    	PCPWM0:1,
    	PCPWM1:1,
    	PCI2C0:1,
    	PCUART4:1,
    	PCRTC:1,
    	PCSSP1:1,
    	PCEMC:1,
    	PCADC:1,
    	PCCAN1:1,
    	PCCAN2:1,
    	PCGPIO:1,
    	PCSPIFI:1,
    	PCMCPWM:1,
    	PCQEI:1,
    	PCI2C1:1,
    	PCSSP2:1,
    	PCSSP0:1,
    	PCTIM2:1,
    	PCTIM3:1,
    	PCUART2:1,
        PCUART3:1,
        PCI2C2:1,
        PCI2S:1,
        PCSDS:1,
        PCFPDMA:1,
        PCENET:1,
        PCUSB:1;
    };

}PCONP_typedef;


typedef union{
     uint8_t LCR;
struct{
     uint8_t     
     WLS                     :2,   
     SBS                     :1,
     PE                      :1,
     PS                      :2,
     BC                      :1,
     DLAB                    :1;          
     };
}LCR_typedef;
